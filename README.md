Start the service
---------------

## requirements

- docker
- docker-compose

## Installation guide

1. run `docker-compose up -d`
2. check `docker-compose logs -f` command to see the images logs

## To debug phantom-traverse.js file
1. enter in traverser docker `docker exec -it traverser bash`
2. go in lib folder`cd lib`
3. run `phantomjs phantom-traverse.js` command (e.g. `phantomjs phantom-traverse.js http://www.uniba.it/`)
